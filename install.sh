#!/usr/bin/env bash

# Install server dependencies
# Install server
# Run tests

cd server  # this is the server folder

echo "> installing dependencies"
npm install  # install dependencies

echo "> installing www"
cd node_modules  # install express server
mkdir build
cd build
express
cd .. && cd ..  # get back to server folder
mkdir bin
mv node_modules/build/bin/www bin/

echo "> cleaning"
rm -rf node_modules/build

echo "> testing"
npm run test  # run tests
