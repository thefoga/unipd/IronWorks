<div align="center">
<h1>WarMachine | IronWorks</h1>
<em>Costruire software robusto</em></br></br>

<a href="https://travis-ci.com/sirfoga/IronWorks"><img src="https://travis-ci.com/sirfoga/IronWorks.svg?token=2ioPzyypRgPWTrC2NfJK&branch=master"></a>
<a href="http://unlicense.org/"><img src="https://img.shields.io/badge/license-Unlicense-blue.svg"></a>
</div>

## Install
In most cases you should be up and running with just

```shell
npm install
```
but for a complete and clean installation you should use `bash install.sh`

### Dependencies (see [package.json](server/package.json))
- `archiver` to create `tar` and `zip` (**required** to run the server)
- `body-parser` to parse GET/POST requests (**required** to run the server)
- `express` for the server (**required** to run the server)
- `istanbul` for code coverage
- `jade` used to render some pages (e.g `404`) (**required** to run the server)
- `jasmine-core` for client-side testing
- `jasmine-node` for server-side testing
- `karma` (and related packages) for client-side testing
- `mocha` for api tests
- `nyc` for api tests reports

## Usage
```shell
$ npm start
```
Then, head over to [your browser](http://127.0.0.1:3000).


## Tests
- server: `npm run server-test` and open [this file](tests/server/coverage/lcov-report/index.html) in a browser, or
simply go [here](https://sirfoga.github.io/IronWorks/server/coverage/lcov-report/index.html)

![server test coverage](res/coverage/server.png)
*Excerpt of server test coverage report*

- client: `npm run client-test` and open [this file](tests/client/coverage/html/index.html) in a browser, or simply go [here](https://sirfoga.github.io/IronWorks/client/coverage/html/index.html)

![client test coverage](res/coverage/client.png)
*Excerpt of client test coverage report*

- api (HTTP GET and POST): `npm run api-test` and open [this file](tests/api/index.html) in a, or simply go [here](https://sirfoga.github.io/IronWorks/api/index.html)
browser

![api test coverage](res/coverage/api.png)
*Excerpt of api test coverage report*

## Contributing
[Fork](https://github.com/sirfoga/IronWorks/fork) | Patch | Push | [Pull request](https://github.com/sirfoga/IronWorks/pulls)


## Feedback
Suggestions and improvements [welcome](https://github.com/sirfoga/IronWorks/issues)!


## Authors
| [izanetti](https://github.com/izanetti "Follow @izanetti on Github") | [elezanon](https://github.com/elezanon "Follow @elezanon on Github") | [![Bragaz](https://avatars1.githubusercontent.com/u/26248202?s=128&v=4)](https://github.com/Bragaz "Follow @Bragaz Github") | [![acoletti-33100](https://avatars1.githubusercontent.com/u/32634632?s=128&v=4)](https://github.com/acoletti-33100 "Follow @acoletti-33100 on Github") | [![ShiroAimai](https://avatars0.githubusercontent.com/u/33904974?s=128&v=4)](https://github.com/ShiroAimai "Follow @ShiroAimai on Github") | [riccardobek](https://github.com/riccardobek "Follow @riccardobek on Github") | [![sirfoga](https://avatars0.githubusercontent.com/u/14162628?s=128&v=4)](https://github.com/sirfoga "Follow @sirfoga on Github") |
|---|---|---|---|---|---|---|
| [Ilenia Zanetti](https://github.com/izanetti) | [Elena Zanon](https://github.com/elezanon) | [Leonardo Bragagnolo](https://github.com/Bragaz) | [Andrea Coletti](https://github.com/acoletti-33100) | [Nicola Cisternino](https://github.com/ShiroAimai) | [Riccardo Bernucci](https://github.com/riccardobek) | [Stefano Fogarollo](https://sirfoga.github.io) |


## License
[Unlicense](https://unlicense.org/)
