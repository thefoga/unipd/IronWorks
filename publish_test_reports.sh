#!/usr/bin/env bash

# Commits tests reports in publish branch
# Returns to main branch

PUBLISH_BRANCH="gh-pages"
BUILD_FOLDER="tests/"
BACKUP_ROOT="server"
TMP_FOLDER="${HOME}/tmp/ironworks-backup"
COMMIT_MSG=$(git log -1 --pretty=%B)  # last commit message
COMMIT_MSG="${COMMIT_MSG} (generated docs)"  # add docs notice

echo "> generating tests"
rm -rf ${BUILD_FOLDER}
cd ${BACKUP_ROOT}
npm run test
cd .. # get back to root

echo "> creating backup"
rm -rf ${TMP_FOLDER}
mkdir ${TMP_FOLDER}
cp -r ${BACKUP_ROOT}/bin/ ${TMP_FOLDER}/bin/
cp -r ${BACKUP_ROOT}/node_modules/ ${TMP_FOLDER}/node_modules/
cp ${BACKUP_ROOT}/.openode ${TMP_FOLDER}/.openode

echo "> saving tests"
cp -r ${BUILD_FOLDER} ${TMP_FOLDER}/${BUILD_FOLDER}

echo "> moving to docs branch"
git checkout ${PUBLISH_BRANCH}  # change branch (to publish docs)
cp -r ${TMP_FOLDER}/${BUILD_FOLDER}* . # copy to root

echo "> committing changes"
git add --all
git commit -m "${COMMIT_MSG} (generated tests reports)"
git push origin ${PUBLISH_BRANCH}

echo "> exiting..."
git checkout master

echo "> restore backup"
rm -rf ${BACKUP_ROOT}/bin/
cp -r ${TMP_FOLDER}/bin/ ${BACKUP_ROOT}/bin/

rm -rf ${BACKUP_ROOT}/node_modules/
cp -r ${TMP_FOLDER}/node_modules/ ${BACKUP_ROOT}/node_modules/

rm ${BACKUP_ROOT}/.openode
cp ${TMP_FOLDER}/.openode ${BACKUP_ROOT}/.openode

echo "> cleaning"
rm -rf ${TMP_FOLDER}
rm -rf ${BUILD_FOLDER}
