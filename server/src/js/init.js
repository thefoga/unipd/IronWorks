const VERSION = '6.7.5 (79f83e3849686796509ef04b2893432f08085320 at 21:56:37 18-08-22 +0200)'

/**
 * Prints to screen and logs in console current version
 */
function logVersion () {
  try {
    document.getElementById('version').innerHTML = VERSION
  } catch (error) {
  }
  console.log(VERSION)
}

logVersion()
